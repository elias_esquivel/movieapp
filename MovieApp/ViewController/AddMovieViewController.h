//
//  AddMovieViewController.h
//  MovieApp
//
//  Created by Elias Esquivel on 10/10/15.
//  Copyright © 2015 Elias Esquivel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

extern NSString * const AMVCIdentifier;

@interface AddMovieViewController : UIViewController

@end
