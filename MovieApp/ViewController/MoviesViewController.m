//
//  MoviesViewController.m
//  MovieApp
//
//  Created by Elias Esquivel on 10/10/15.
//  Copyright © 2015 Elias Esquivel. All rights reserved.
//

#import "MoviesViewController.h"
#import "DetailViewController.h"
#import "MovieTableViewCell.h"
#import "AddMovieViewController.h"

@interface MoviesViewController ()<UIPopoverPresentationControllerDelegate, NSFetchedResultsControllerDelegate>

@property (nonatomic, strong)NSArray *movies;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property(nonatomic,retain)UIPopoverPresentationController *addMoviePopOver;
@end

@implementation MoviesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fetchData)
                                                 name:NOTIFICATION_RELOAD_DATA object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self fetchData];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_RELOAD_DATA object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -mark IBActions

- (IBAction)showAddMovies:(id)sender
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:Storyboard_Name bundle:nil];
    UIViewController *addMovie = [sb instantiateViewControllerWithIdentifier:AMVCIdentifier];
    
    
    UINavigationController *destNav = [[UINavigationController alloc] initWithRootViewController:addMovie];
    addMovie.preferredContentSize = CGSizeMake(self.view.frame.size.width ,self.view.frame.size.height);
    destNav.modalPresentationStyle = UIModalPresentationPopover;
    self.addMoviePopOver = destNav.popoverPresentationController;
    self.addMoviePopOver.delegate = self;
    self.addMoviePopOver.sourceView = self.view;
    self.addMoviePopOver.permittedArrowDirections= 0;
    self.addMoviePopOver.popoverLayoutMargins = UIEdgeInsetsMake(20, 20, 20, 20);
    destNav.navigationBarHidden = YES;
    [self presentViewController:destNav animated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[_fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MovieTableViewCell *cell = (MovieTableViewCell *)[tableView dequeueReusableCellWithIdentifier:MTVCIdentifier forIndexPath:indexPath];
    
    
    Movie *movie = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if (cell) {
        [cell configureCell:movie];
    }
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Movie *movie = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            Movie *localMovie = [movie MR_inContext:localContext];
            [localMovie MR_deleteEntity];
        } completion:^(BOOL contextDidSave, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self fetchData];
            });
        }];
        
    }
}

#pragma -mark Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:DVCSegueID]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        DetailViewController *detail = [segue destinationViewController];
        detail.movie = [self.fetchedResultsController objectAtIndexPath:indexPath];
    }
}

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController: (UIPresentationController * ) controller {
    return UIModalPresentationNone;
}

#pragma -mark NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    _fetchedResultsController = [Movie MR_fetchAllSortedBy:@"dateCreated" ascending:NO withPredicate:nil groupBy:nil delegate:self];
    return _fetchedResultsController;
}

- (void)fetchData
{
    NSError *error;
    
    if (![[self fetchedResultsController] performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    
    [self.tableView reloadData];
}


@end
