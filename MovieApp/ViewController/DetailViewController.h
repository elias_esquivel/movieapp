//
//  DetailViewController.h
//  MovieApp
//
//  Created by Elias Esquivel on 10/10/15.
//  Copyright © 2015 Elias Esquivel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movie.h"

extern NSString * const DVCSegueID;

@interface DetailViewController : UIViewController

@property (nonatomic, strong)Movie *movie;

@end
