//
//  DetailViewController.m
//  MovieApp
//
//  Created by Elias Esquivel on 10/10/15.
//  Copyright © 2015 Elias Esquivel. All rights reserved.
//

#import "DetailViewController.h"
#import "HCSStarRatingView.h"
#import "Rate.h"

NSString * const DVCSegueID = @"detailSegue";

@interface DetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblMovieName;
@property (weak, nonatomic) IBOutlet UILabel *lblTimesRated;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *viewStarRating;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.lblMovieName setText:self.movie.name];
    [self.lblTimesRated setText:[self.movie.timesRated stringValue]];
    NSInteger movieScore = 0;
    if (self.movie.rated) {
        movieScore = [self.movie.rated.score integerValue] / [self.movie.timesRated integerValue];
    }
    [self.viewStarRating setValue:movieScore];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -mark IBActions

- (IBAction)didChangeValue:(id)sender
{
    
}

- (IBAction)didRate:(id)sender
{
    [Movie addRate:self.viewStarRating.value toMovie:self.movie];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
