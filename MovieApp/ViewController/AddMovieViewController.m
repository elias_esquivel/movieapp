//
//  AddMovieViewController.m
//  MovieApp
//
//  Created by Elias Esquivel on 10/10/15.
//  Copyright © 2015 Elias Esquivel. All rights reserved.
//

#import "AddMovieViewController.h"
#import "Movie.h"
#import "Rate.h"
#import "MovieCategory.h"
#import <MagicalRecord/MagicalRecord.h>

NSString * const AMVCIdentifier = @"addMovieVCID";

@interface AddMovieViewController ()<UIPickerViewDelegate, UITextFieldDelegate> 

@property (weak, nonatomic) IBOutlet UITextField *txtViewMovieName;
@property (weak, nonatomic) IBOutlet UIPickerView *pckrCategory;
@property (strong, nonatomic) NSArray *categories;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@end

@implementation AddMovieViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadData];
    [self configureView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


#pragma -mark Configuration Methods

- (void)configureView
{
    self.pckrCategory.delegate = self;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(hidekeyboard)];
    [self.view addGestureRecognizer:tap];
}

- (void)loadData
{
    self.categories = [MovieCategory MR_findAllSortedBy:@"categoryName" ascending:YES];
}

#pragma -mark IBActions

- (IBAction)dismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)addMovie:(id)sender
{
    if (![self.txtViewMovieName.text isEqualToString:@""]) {
        NSInteger index = [self.pckrCategory selectedRowInComponent:0];
        MovieCategory *category = [self.categories objectAtIndex:(NSUInteger)index];
        
        [Movie addMovie:self.txtViewMovieName.text withCategory:category];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_RELOAD_DATA object:nil];

    }
    
    [self dismiss:nil];
}

#pragma -mark Picker Delegate methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (self.categories) {
        return [self.categories count];
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *name = @"";
    
    if (self.categories) {
        name = ((MovieCategory *)[self.categories objectAtIndex:row]).categoryName;
    }
    return name;
}

#pragma -mark TextField Delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self hidekeyboard];
    return YES;
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}

- (void)keyboardDidShow:(NSNotification *)notification
{
    [UIView animateWithDuration:0.9
                          delay:1.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [self.addButton setHidden:YES];
                     }
                     completion:^(BOOL finished){
                         
                     }];
    
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.1
                          delay:0.1
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         [self.addButton setHidden:NO];
                     }
                     completion:^(BOOL finished){
                         
                     }];
    
}

-(void)hidekeyboard
{
    [self.txtViewMovieName resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
