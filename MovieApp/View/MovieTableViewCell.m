//
//  MovieTableViewCell.m
//  MovieApp
//
//  Created by Elias Esquivel on 10/10/15.
//  Copyright © 2015 Elias Esquivel. All rights reserved.
//

#import "MovieTableViewCell.h"

NSString * const MTVCIdentifier = @"movieTVCell";

@interface MovieTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblMovieCategory;
@property (weak, nonatomic) IBOutlet UILabel *lblRate;

@end

@implementation MovieTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureCell:(Movie *)movie
{
    [self.lblName setText:movie.name];
    [self.lblMovieCategory setText:movie.oneCategory.categoryName];

    NSInteger movieScore = 0;
    if (movie.rated) {
        movieScore = [movie.rated.score integerValue] / [movie.timesRated integerValue];
    }
   [self.lblRate setText:[NSString stringWithFormat:@"%ld", (long)movieScore]];
}

@end
