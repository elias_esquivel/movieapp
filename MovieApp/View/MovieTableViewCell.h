//
//  MovieTableViewCell.h
//  MovieApp
//
//  Created by Elias Esquivel on 10/10/15.
//  Copyright © 2015 Elias Esquivel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movie.h"
#import "MovieCategory.h"
#import "Rate.h"

extern NSString * const MTVCIdentifier;

@interface MovieTableViewCell : UITableViewCell

- (void)configureCell:(Movie *)movie;

@end
