#import "Movie.h"

@interface Movie ()

// Private interface goes here.

@end

@implementation Movie

+ (void)addRate:(NSInteger )score toMovie:(Movie *)movie;
{
    Rate *rate = [Rate MR_createEntity];
    rate.dateCreated = [NSDate date];
    
    NSInteger totalScore = [movie.rated.score integerValue] + score;
    NSInteger timesRated = [movie.timesRated integerValue] + 1;
    
    movie.timesRated = [NSNumber numberWithInteger:timesRated];
    
    rate.score = [NSNumber numberWithInteger:totalScore];
    movie.rated = rate;
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

+ (void)addMovie:(NSString *)name withCategory:(MovieCategory *)category
{
    MovieCategory *selectedCategory = [MovieCategory MR_findFirstByAttribute:@"categoryName" withValue:category.categoryName];
    
    Movie *movie = [Movie MR_createEntity];
    movie.name = name;
    movie.dateCreated = [NSDate date];
    movie.oneCategory = selectedCategory;
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

@end
