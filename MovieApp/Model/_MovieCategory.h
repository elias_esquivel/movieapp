// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MovieCategory.h instead.

#import <CoreData/CoreData.h>

extern const struct MovieCategoryAttributes {
	__unsafe_unretained NSString *categoryName;
} MovieCategoryAttributes;

extern const struct MovieCategoryRelationships {
	__unsafe_unretained NSString *manyMovies;
} MovieCategoryRelationships;

@class Movie;

@interface MovieCategoryID : NSManagedObjectID {}
@end

@interface _MovieCategory : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) MovieCategoryID* objectID;

@property (nonatomic, strong) NSString* categoryName;

//- (BOOL)validateCategoryName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Movie *manyMovies;

//- (BOOL)validateManyMovies:(id*)value_ error:(NSError**)error_;

@end

@interface _MovieCategory (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveCategoryName;
- (void)setPrimitiveCategoryName:(NSString*)value;

- (Movie*)primitiveManyMovies;
- (void)setPrimitiveManyMovies:(Movie*)value;

@end
