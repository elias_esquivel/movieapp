// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Movie.m instead.

#import "_Movie.h"

const struct MovieAttributes MovieAttributes = {
	.dateCreated = @"dateCreated",
	.name = @"name",
	.timesRated = @"timesRated",
};

const struct MovieRelationships MovieRelationships = {
	.oneCategory = @"oneCategory",
	.rated = @"rated",
};

@implementation MovieID
@end

@implementation _Movie

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Movie" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Movie";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Movie" inManagedObjectContext:moc_];
}

- (MovieID*)objectID {
	return (MovieID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"timesRatedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"timesRated"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic dateCreated;

@dynamic name;

@dynamic timesRated;

- (int32_t)timesRatedValue {
	NSNumber *result = [self timesRated];
	return [result intValue];
}

- (void)setTimesRatedValue:(int32_t)value_ {
	[self setTimesRated:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveTimesRatedValue {
	NSNumber *result = [self primitiveTimesRated];
	return [result intValue];
}

- (void)setPrimitiveTimesRatedValue:(int32_t)value_ {
	[self setPrimitiveTimesRated:[NSNumber numberWithInt:value_]];
}

@dynamic oneCategory;

@dynamic rated;

@end

