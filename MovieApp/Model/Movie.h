#import "_Movie.h"
#import "Rate.h"
#import "MovieCategory.h"

@interface Movie : _Movie {}

+ (void)addMovie:(NSString *)name withCategory:(MovieCategory *)category;
+ (void)addRate:(NSInteger )score toMovie:(Movie *)movie;

@end
