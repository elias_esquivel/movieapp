// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Rate.m instead.

#import "_Rate.h"

const struct RateAttributes RateAttributes = {
	.dateCreated = @"dateCreated",
	.isEnabled = @"isEnabled",
	.score = @"score",
};

const struct RateRelationships RateRelationships = {
	.movie = @"movie",
};

@implementation RateID
@end

@implementation _Rate

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Rate" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Rate";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Rate" inManagedObjectContext:moc_];
}

- (RateID*)objectID {
	return (RateID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"isEnabledValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isEnabled"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"scoreValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"score"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic dateCreated;

@dynamic isEnabled;

- (BOOL)isEnabledValue {
	NSNumber *result = [self isEnabled];
	return [result boolValue];
}

- (void)setIsEnabledValue:(BOOL)value_ {
	[self setIsEnabled:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsEnabledValue {
	NSNumber *result = [self primitiveIsEnabled];
	return [result boolValue];
}

- (void)setPrimitiveIsEnabledValue:(BOOL)value_ {
	[self setPrimitiveIsEnabled:[NSNumber numberWithBool:value_]];
}

@dynamic score;

- (int16_t)scoreValue {
	NSNumber *result = [self score];
	return [result shortValue];
}

- (void)setScoreValue:(int16_t)value_ {
	[self setScore:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveScoreValue {
	NSNumber *result = [self primitiveScore];
	return [result shortValue];
}

- (void)setPrimitiveScoreValue:(int16_t)value_ {
	[self setPrimitiveScore:[NSNumber numberWithShort:value_]];
}

@dynamic movie;

@end

