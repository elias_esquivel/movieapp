// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Rate.h instead.

#import <CoreData/CoreData.h>

extern const struct RateAttributes {
	__unsafe_unretained NSString *dateCreated;
	__unsafe_unretained NSString *isEnabled;
	__unsafe_unretained NSString *score;
} RateAttributes;

extern const struct RateRelationships {
	__unsafe_unretained NSString *movie;
} RateRelationships;

@class Movie;

@interface RateID : NSManagedObjectID {}
@end

@interface _Rate : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) RateID* objectID;

@property (nonatomic, strong) NSDate* dateCreated;

//- (BOOL)validateDateCreated:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isEnabled;

@property (atomic) BOOL isEnabledValue;
- (BOOL)isEnabledValue;
- (void)setIsEnabledValue:(BOOL)value_;

//- (BOOL)validateIsEnabled:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* score;

@property (atomic) int16_t scoreValue;
- (int16_t)scoreValue;
- (void)setScoreValue:(int16_t)value_;

//- (BOOL)validateScore:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Movie *movie;

//- (BOOL)validateMovie:(id*)value_ error:(NSError**)error_;

@end

@interface _Rate (CoreDataGeneratedPrimitiveAccessors)

- (NSDate*)primitiveDateCreated;
- (void)setPrimitiveDateCreated:(NSDate*)value;

- (NSNumber*)primitiveIsEnabled;
- (void)setPrimitiveIsEnabled:(NSNumber*)value;

- (BOOL)primitiveIsEnabledValue;
- (void)setPrimitiveIsEnabledValue:(BOOL)value_;

- (NSNumber*)primitiveScore;
- (void)setPrimitiveScore:(NSNumber*)value;

- (int16_t)primitiveScoreValue;
- (void)setPrimitiveScoreValue:(int16_t)value_;

- (Movie*)primitiveMovie;
- (void)setPrimitiveMovie:(Movie*)value;

@end
