// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Movie.h instead.

#import <CoreData/CoreData.h>

extern const struct MovieAttributes {
	__unsafe_unretained NSString *dateCreated;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *timesRated;
} MovieAttributes;

extern const struct MovieRelationships {
	__unsafe_unretained NSString *oneCategory;
	__unsafe_unretained NSString *rated;
} MovieRelationships;

@class MovieCategory;
@class Rate;

@interface MovieID : NSManagedObjectID {}
@end

@interface _Movie : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) MovieID* objectID;

@property (nonatomic, strong) NSDate* dateCreated;

//- (BOOL)validateDateCreated:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* timesRated;

@property (atomic) int32_t timesRatedValue;
- (int32_t)timesRatedValue;
- (void)setTimesRatedValue:(int32_t)value_;

//- (BOOL)validateTimesRated:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) MovieCategory *oneCategory;

//- (BOOL)validateOneCategory:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Rate *rated;

//- (BOOL)validateRated:(id*)value_ error:(NSError**)error_;

@end

@interface _Movie (CoreDataGeneratedPrimitiveAccessors)

- (NSDate*)primitiveDateCreated;
- (void)setPrimitiveDateCreated:(NSDate*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitiveTimesRated;
- (void)setPrimitiveTimesRated:(NSNumber*)value;

- (int32_t)primitiveTimesRatedValue;
- (void)setPrimitiveTimesRatedValue:(int32_t)value_;

- (MovieCategory*)primitiveOneCategory;
- (void)setPrimitiveOneCategory:(MovieCategory*)value;

- (Rate*)primitiveRated;
- (void)setPrimitiveRated:(Rate*)value;

@end
