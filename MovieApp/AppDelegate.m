//
//  AppDelegate.m
//  MovieApp
//
//  Created by Elias Esquivel on 10/10/15.
//  Copyright © 2015 Elias Esquivel. All rights reserved.
//

#import "AppDelegate.h"
#import "MovieCategory.h"
#import "Rate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [MagicalRecord setupCoreDataStack];
    
   // UNCOMMENT THIS CODE TO CREATE INITIAL CATEGORIES
    
   /* MovieCategory *category = [MovieCategory MR_createEntity];
    category.categoryName = @"Accion";
    
    MovieCategory *category2 = [MovieCategory MR_createEntity];
    category2.categoryName = @"Historia";
    
    
    MovieCategory *category3 = [MovieCategory MR_createEntity];
    category3.categoryName = @"Animadas";
    
    MovieCategory *category4 = [MovieCategory MR_createEntity];
    category4.categoryName = @"Suspenso";
    
    MovieCategory *category5 = [MovieCategory MR_createEntity];
    category5.categoryName = @"Terror";
    
    NSManagedObjectContext *defaultContext = [NSManagedObjectContext MR_defaultContext];
    
    [defaultContext MR_saveToPersistentStoreAndWait];*/
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
