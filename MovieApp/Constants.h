//
//  Constants.h
//  MovieApp
//
//  Created by Elias Esquivel on 10/10/15.
//  Copyright © 2015 Elias Esquivel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject

extern NSString * const NOTIFICATION_RELOAD_DATA;
extern NSString * const Storyboard_Name;

@end

